package task08.model;

import java.util.Arrays;

import static task08.view.Main.logger;

@MyAnnotation(studentHome = "Bandery", course = 3)
public class AnnotationUtils {
    private static String name;
    public int age;
    protected boolean flag;


    public void withOneParamert(String a) {
        logger.info("This is method: withOneParametrs(" + a + ")");
    }

    public void withTwoParametr(int c, String... a) {
        logger.info("This is method: withTwoParametrs(" + c + ")(" + Arrays.toString(a) + ")");
    }

    @MyAnnotation(studentHome = "NotBandery", course = 4)
    private void someAnother() {
        logger.info("This is method: someAnother()");
    }
}