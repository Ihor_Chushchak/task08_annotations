package task08.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task08.model.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@MyAnnotation(studentHome = "EPAM", course = 5)
public class Main {
    private static String name = "Petro";
    public static Logger logger = LogManager.getLogger(task08.view.Main.class);

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {

        Class<Main> mainClass = Main.class;

        if (mainClass.isAnnotationPresent(MyAnnotation.class)) {
            MyAnnotation annotation = mainClass.getAnnotation(MyAnnotation.class);
            Field[] mas = mainClass.getDeclaredFields();

            for (Field field : mas) {
                logger.info("Field \"" + field.getName() + "\" before: " + field.get(field.getName()));
                if (field.get(field.getName()) instanceof String) {
                    String v = (String) field.get(field.getName());
                    field.set(field.getName(), "Ihor");
                    logger.info("Field \"" + field.getName() + "\" afrer: " + field.get(field.getName()));
                }
            }

            Annotation[] annotations = MyAnnotation.class.getAnnotations();


            for (Annotation ant : annotations) {
                Field[] pol = ant.annotationType().getDeclaredFields();
                for (Field p : pol) {
                    logger.info(p.getName());
                }
            }
            logger.info("\n\nAnnotations fields::");
            logger.info(annotation.studentName());
            logger.info(annotation.studentHome());
            logger.info(annotation.studentAge());
            logger.info(annotation.course());
        } else {
            logger.info("there is zero annotations!...badddddly");
        }

        Class annotationUtilsClass = AnnotationUtils.class;
        AnnotationUtils myObj = new AnnotationUtils();

        Annotation[] ann = annotationUtilsClass.getAnnotations();
        logger.info("\n\nAll annotations from class:    " + annotationUtilsClass.getName());
        for (Annotation i : ann) {
            logger.info(i.toString());
        }
        logger.info("\n\nInformation about fields: ");
        Field[] myFields = annotationUtilsClass.getDeclaredFields();
        for (Field i : myFields) {
            i.setAccessible(true);
            logger.info("Field: " + i.getName() + ", type: " + i.getType().toString());
        }
        logger.info("\n\nInformation about methods:");
        Method[] myMethod = annotationUtilsClass.getDeclaredMethods();
        for (Method i : myMethod) {
            logger.info("Method: " + i.getName());
            if (i.getName().equals("withOneParametr")) {
                i.invoke(myObj, "Hello");
            }
        }
    }
}
